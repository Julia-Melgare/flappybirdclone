using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] pipePrefabs;
    public Bird bird;
    private bool startedGame = false;
    public int score { get; private set; }
    private GameObject pipesPrefab;
    public SkinManager skinManager;

    public delegate void OnGameStart();
    public static event OnGameStart onGameStart;
    public delegate void OnUpdateScore(int score);
    public static event OnUpdateScore onUpdateScore;
    public delegate void OnGamePause();
    public static event OnGamePause onGamePause;
    public delegate void OnGameResume();
    public static event OnGameResume onGameResume;
    public delegate void OnGameOver();
    public static event OnGameOver onGameOver;

#if UNITY_EDITOR || UNITY_STANDALONE
    private bool gamePaused;
#endif

    private void Awake()
    {
        Bird.onBirdHit += GameOver;
        Bird.onBirdScore += CountScore;
    }

    private void OnDisable()
    {
        Bird.onBirdHit -= GameOver;
        Bird.onBirdScore -= CountScore;
    }

    private void Start()
    {
        Time.timeScale = 1f;
        score = 0;
        pipesPrefab = pipePrefabs[PlayerPrefs.GetInt("difficulty", 0)]; //default to normal dificulty
        bird.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
    }

    private void Update()
    {
        if (!startedGame)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            {
                onGameStart?.Invoke();
                startedGame = true;
                bird.GetComponent<Rigidbody2D>().isKinematic = false;
                Time.timeScale = 1f;
                UpdatePipesSkin(skinManager.pipesSkin);
                StartCoroutine(PipeSpawner());
            }
        }
        else
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!gamePaused)
                {
                    Pause();
                    gamePaused = true;
                }
                else{
                    Resume();
                    gamePaused = false;
                }
                
            }
#endif
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        onGamePause?.Invoke();
    }

    public void Resume()
    {
        onGameResume?.Invoke();
        Time.timeScale = 1f;        
    }

    private void GameOver()
    {
        bird.gameObject.GetComponent<PlayerInput>().enabled = false;
        UpdateHighScore();
        bird.gameObject.GetComponent<Collider2D>().enabled = false;
        bird.movement.rb.constraints = RigidbodyConstraints2D.None;
        bird.movement.rb.velocity = Vector3.down * 4f;
        AudioManager.instance.PlaySoundEffect(bird.dieAudioClip);
        onGameOver?.Invoke();
    }

    private void CountScore()
    {
        score++;
        onUpdateScore?.Invoke(score);
    }

    private void UpdateHighScore()
    {
        if(score > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", score);
        }
    }

    private void SpawnPipes()
    {
        GameObject pipes = Instantiate(pipesPrefab);
        float pipeHeight = Random.Range(-2f, 2f);
        pipes.transform.position += Vector3.up * pipeHeight;
        Destroy(pipes, 5f);
    }

    private void UpdatePipesSkin(Sprite pipeSkin)
    {
        foreach(SpriteRenderer spriteRenderer in pipesPrefab.GetComponentsInChildren(typeof(SpriteRenderer)))
        {
            spriteRenderer.sprite = pipeSkin;
        }
    }

    private IEnumerator PipeSpawner()
    {
        yield return new WaitForSeconds(1.5f);
        SpawnPipes();
        StartCoroutine(PipeSpawner());
    }
}
