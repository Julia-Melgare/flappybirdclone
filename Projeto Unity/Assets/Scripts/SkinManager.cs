using UnityEngine;

public class SkinManager : MonoBehaviour
{
    public RuntimeAnimatorController[] birdSkins;
    public Sprite[] backgroundSkins;
    public Sprite[] pipesSkins;

    public Animator birdAnimator;
    public SpriteRenderer backgroundSprite;
    public Sprite pipesSkin { get; private set; }

    private void Start()
    {
        int birdSkinIndex = Random.Range(0, birdSkins.Length);
        birdAnimator.runtimeAnimatorController = birdSkins[birdSkinIndex];

        int backgroundSkinIndex = Random.Range(0, backgroundSkins.Length);
        backgroundSprite.sprite = backgroundSkins[backgroundSkinIndex];
        pipesSkin = pipesSkins[backgroundSkinIndex];
    }

}
