using UnityEngine;
using TMPro;

public class MenuUIManager : MonoBehaviour
{
    public GameObject highScoreCanvas;
    public TMP_Dropdown difficultyDropdown;
    public TMP_Text highScoreText;


    private void Start()
    {
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("difficulty", difficultyDropdown.value); //initialize difficulty to normal
        difficultyDropdown.onValueChanged.AddListener(delegate {
            DifficultyChanged(difficultyDropdown);
        });

        if (PlayerPrefs.HasKey("highscore"))
        {
            highScoreText.text = PlayerPrefs.GetInt("highscore").ToString();
            highScoreCanvas.SetActive(true);
        }
    }

    private void DifficultyChanged(TMP_Dropdown change)
    {
        PlayerPrefs.SetInt("difficulty", change.value);
    }
}
