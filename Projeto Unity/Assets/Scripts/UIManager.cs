using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class UIManager : MonoBehaviour
{
    public TMP_Text scoreText;
    public TMP_Text gameOverScoreText;
    public TMP_Text gameOverHighScoreText;
    public GameObject tutorialScreen;
    public GameObject gameOverScreen;
    public GameObject pauseScreen;
    public Button pauseButton;

    private void Awake()
    {
        GameManager.onGameStart += HideTutorialScreen;
        GameManager.onUpdateScore += UpdateScoreText;
        GameManager.onGamePause += Pause;
        GameManager.onGameResume += Resume;
        GameManager.onGameOver += GameOver;

        tutorialScreen.GetComponent<Animator>().enabled = false;
    }

    private void OnDisable()
    {
        GameManager.onGameStart -= HideTutorialScreen;
        GameManager.onUpdateScore -= UpdateScoreText;
        GameManager.onGamePause -= Pause;
        GameManager.onGameResume -= Resume;
        GameManager.onGameOver -= GameOver;
    }

    private void UpdateScoreText(int score)
    {
        scoreText.text = score.ToString();
    }

    private void HideTutorialScreen()
    {
        StartCoroutine(TutorialUIAnim());        
    }

    private void Pause()
    {
        pauseButton.gameObject.SetActive(false);
        pauseScreen.SetActive(true);
    }

    private void Resume()
    {
        pauseScreen.SetActive(false);
        pauseButton.gameObject.SetActive(true);
    }

    private void GameOver()
    {
        gameOverScreen.SetActive(true);
        StartCoroutine(GameOverUIAnim());
        gameOverScoreText.text = scoreText.text;
        gameOverHighScoreText.text = PlayerPrefs.GetInt("highscore").ToString();
    }

    private IEnumerator GameOverUIAnim()
    {
        yield return new WaitForSecondsRealtime(0.75f);
        Time.timeScale = 0f;
    }

    private IEnumerator TutorialUIAnim()
    {
        tutorialScreen.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(0.3f);
        tutorialScreen.SetActive(false);
    }
}
