using UnityEngine;

public class PipeMovement : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        gameObject.transform.position += Vector3.left * speed * Time.deltaTime;
    }
}
