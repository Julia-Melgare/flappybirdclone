using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class Bird : MonoBehaviour
{
    public delegate void OnBirdHit();
    public static event OnBirdHit onBirdHit;

    public delegate void OnBirdScore();
    public static event OnBirdScore onBirdScore;

    public PlayerInput movement { get; private set; }

    public AudioClip scoreAudioClip;
    public AudioClip hitAudioClip;
    public AudioClip dieAudioClip;

    private void Start()
    {
        movement = gameObject.GetComponent<PlayerInput>();
    }

    private void Update()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 0, movement.rb.velocity.y * 5f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Pipe" || collision.gameObject.tag == "Ground")
        {
            onBirdHit?.Invoke();
            AudioManager.instance.PlaySoundEffect(hitAudioClip);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Score")
        {
            onBirdScore?.Invoke();
            AudioManager.instance.PlaySoundEffect(scoreAudioClip);
        }
    }
}
