# Flappy Bird Clone

Flappy Bird clone game made in Unity. Builds for both PC and Android are available to download in the [Releases](https://gitlab.com/Julia-Melgare/flappybirdclone/-/releases).

The game replicates the features of the original game (except for the ranking and medal system), and provides three different difficulty settings which affect the gap between the pipes (Easy - larger space, Normal - original game space, Hard - tighter space).

All sprites present in this game that are not part of the original game, such as the quit button and menu button, were created by the author, Julia Melgare. 
